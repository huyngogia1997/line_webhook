FROM tomcat:9.0-jdk8-openjdk

ADD ./build/libs/ /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]
