package com.bhx.linewebhook.common;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import org.springframework.stereotype.Component;

import java.text.Normalizer;
import java.util.Collection;

@Component
public class StringUtils {
    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    public static ExtractedResult searchFuzzy(String message, Collection<String> metaDatas) {
        return FuzzySearch.extractSorted(stripAccents(message).toLowerCase(), metaDatas).get(0);
    }
}
