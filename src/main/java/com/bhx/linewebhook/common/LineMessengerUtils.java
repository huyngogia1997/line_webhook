package com.bhx.linewebhook.common;

import com.linecorp.bot.client.LineBlobClient;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.response.BotApiResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.singletonList;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LineMessengerUtils {
    private final LineMessagingClient lineMessagingClient;
    private final LineBlobClient lineBlobClient;

    public void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, singletonList(message));
    }

    public void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        reply(replyToken, messages, false);
    }

    public void reply(@NonNull String replyToken,
                      @NonNull List<Message> messages,
                      boolean notificationDisabled) {
        try {

            BotApiResponse apiResponse = lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, messages, notificationDisabled))
                    .get();
            log.info("Sent messages: {}", apiResponse);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void replyText(@NonNull String replyToken, @NonNull String message) {
        if (replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken must not be empty");
        }
        if (message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "……";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    public Message getMenu(String messageTitle, Map<String, String> mesageActionMap) {

        List<QuickReplyItem> quickReplyItems = new ArrayList<>(mesageActionMap.size());

        QuickReplyItem.QuickReplyItemBuilder quickReplyBuilder = QuickReplyItem.builder();
        mesageActionMap.entrySet().stream().forEach(entry -> {
            quickReplyItems.add(QuickReplyItem.builder().action(new MessageAction(entry.getValue(), entry.getKey())).build());
        });

        final QuickReply quickReply = QuickReply.items(quickReplyItems);

        return TextMessage
                .builder()
                .text(messageTitle)
                .quickReply(quickReply)
                .build();
    }

    public TemplateMessage getGuideLineMessage() {
        ButtonsTemplate buttonsTemplate = new ButtonsTemplate(
                null,
                "Hướng dẫn sử đụng chatbots",
                "Nêu câu hỏi mà bạn muốn hoặc chọn:",
                Arrays.asList(
                        new MessageAction("[!!] Thoát", "!!"),
                        new MessageAction("[??] Bảng hướng dẫn", "??"),
                        new MessageAction("[tvl] Truy vấn lương", "tvl"),
                        new MessageAction("[dn] Đăng nhập", "dn")
                ));
        return new TemplateMessage("alt", buttonsTemplate);
    }
}
