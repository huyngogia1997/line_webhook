package com.bhx.linewebhook.lineprogress.querysalary.handler;

import com.bhx.linewebhook.common.LineMessengerUtils;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerHandlerI;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerProgress;
import com.bhx.linewebhook.lineprogress.conifguration.LineProgress;
import com.bhx.linewebhook.lineprogress.querysalary.enums.SalaryQuery;
import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@LineProgress
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SalaryHandler implements LineMessengerHandlerI<SalaryQuery> {
    private final LineMessengerUtils lineMessengerUtils;

    @Override
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event, LineMessengerProgress<SalaryQuery> data) {
        String message;
        switch (event.getMessage().getText().toLowerCase().trim()) {
            case "bn":
                message = "100k 1 ngày";
                break;
            case "kn":
                message = "Ngày 5 hằng tháng";
                break;
            default:
                message = "Chọn hoặc nhập câu hỏi !";
        }
        Message menuMessage = lineMessengerUtils.getMenu(message, new HashMap<String, String>(2) {{
            put(SalaryQuery.HOW_MUCH.getKey(), "[bn] Bao nhiêu");
            put(SalaryQuery.PAYMENT_TIME.getKey(), "[kn] Khi nào phát lương");
        }});
        lineMessengerUtils.reply(event.getReplyToken(), menuMessage);
    }

    @Override
    public void handleStickerMessageEvent(MessageEvent<StickerMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleLocationMessageEvent(MessageEvent<LocationMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleImageMessageEvent(MessageEvent<ImageMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleAudioMessageEvent(MessageEvent<AudioMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleVideoMessageEvent(MessageEvent<VideoMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleVideoPlayCompleteEvent(VideoPlayCompleteEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleFileMessageEvent(MessageEvent<FileMessageContent> event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleUnfollowEvent(UnfollowEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleUnknownEvent(UnknownEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleFollowEvent(FollowEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleJoinEvent(JoinEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handlePostbackEvent(PostbackEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleBeaconEvent(BeaconEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleMemberJoined(MemberJoinedEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleMemberLeft(MemberLeftEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleMemberLeft(UnsendEvent event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public void handleOtherEvent(Event event, LineMessengerProgress<SalaryQuery> data) {

    }

    @Override
    public List<String> getMetaData() {
        return Arrays.asList(
                "luong", "thu nhap", "thu nhap hang thang", "tvl"
        );
    }

    @Override
    public SalaryQuery getInitState() {
        return null;
    }
}
