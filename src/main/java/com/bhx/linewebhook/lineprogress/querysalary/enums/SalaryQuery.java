package com.bhx.linewebhook.lineprogress.querysalary.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum SalaryQuery {
    HOW_MUCH("bn", "bao nhieu", "luong cua toi", "how much"),
    PAYMENT_TIME("kn", "tra luong khi nao", "khi nao tra luong", "ngay may tra luong");
    private List<String> metaDate;
    private String key;

    SalaryQuery(String key, String... metaDate) {
        this.metaDate = Arrays.asList(metaDate);
        this.key = key;
    }

}
