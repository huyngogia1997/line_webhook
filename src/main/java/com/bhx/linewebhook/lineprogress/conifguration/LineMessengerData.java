package com.bhx.linewebhook.lineprogress.conifguration;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LineMessengerData<T> {
    private LineMessengerHandlerI<T> handler;
    private LineMessengerProgress<T> process;
}
