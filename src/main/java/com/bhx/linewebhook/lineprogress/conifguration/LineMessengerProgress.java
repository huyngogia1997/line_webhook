package com.bhx.linewebhook.lineprogress.conifguration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LineMessengerProgress<T> {
    private T nextProcessKey;
}
