package com.bhx.linewebhook.lineprogress.conifguration;

import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.*;

import java.util.List;

public interface LineMessengerHandlerI<T> {
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event, LineMessengerProgress<T> data);

    public void handleStickerMessageEvent(MessageEvent<StickerMessageContent> event, LineMessengerProgress<T> data);

    public void handleLocationMessageEvent(MessageEvent<LocationMessageContent> event, LineMessengerProgress<T> data);

    public void handleImageMessageEvent(MessageEvent<ImageMessageContent> event, LineMessengerProgress<T> data);

    public void handleAudioMessageEvent(MessageEvent<AudioMessageContent> event, LineMessengerProgress<T> data);

    public void handleVideoMessageEvent(MessageEvent<VideoMessageContent> event, LineMessengerProgress<T> data);

    public void handleVideoPlayCompleteEvent(VideoPlayCompleteEvent event, LineMessengerProgress<T> data);

    public void handleFileMessageEvent(MessageEvent<FileMessageContent> event, LineMessengerProgress<T> data);

    public void handleUnfollowEvent(UnfollowEvent event, LineMessengerProgress<T> data);

    public void handleUnknownEvent(UnknownEvent event, LineMessengerProgress<T> data);

    public void handleFollowEvent(FollowEvent event, LineMessengerProgress<T> data);

    public void handleJoinEvent(JoinEvent event, LineMessengerProgress<T> data);

    public void handlePostbackEvent(PostbackEvent event, LineMessengerProgress<T> data);

    public void handleBeaconEvent(BeaconEvent event, LineMessengerProgress<T> data);

    public void handleMemberJoined(MemberJoinedEvent event, LineMessengerProgress<T> data);

    public void handleMemberLeft(MemberLeftEvent event, LineMessengerProgress<T> data);

    public void handleMemberLeft(UnsendEvent event, LineMessengerProgress<T> data);

    public void handleOtherEvent(Event event, LineMessengerProgress<T> data);

    public List<String> getMetaData();

    public T getInitState();
}
