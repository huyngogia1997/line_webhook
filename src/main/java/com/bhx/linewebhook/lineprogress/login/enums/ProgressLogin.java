package com.bhx.linewebhook.lineprogress.login.enums;

public enum ProgressLogin {
    INIT, SEND_OTP, VERIFY_OTP
}
