package com.bhx.linewebhook.lineprogress.login.handler;

import com.bhx.linewebhook.common.LineMessengerUtils;
import com.bhx.linewebhook.lineprogress.LineMessengerHandler;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerHandlerI;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerProgress;
import com.bhx.linewebhook.lineprogress.conifguration.LineProgress;
import com.bhx.linewebhook.lineprogress.login.enums.ProgressLogin;
import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.message.TextMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

@LineProgress
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginHandler implements LineMessengerHandlerI<ProgressLogin> {
    private final LineMessengerUtils lineMessengerUtils;

    @Override
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event, LineMessengerProgress<ProgressLogin> data) {
        switch (data.getNextProcessKey()) {
            case INIT:
                lineMessengerUtils.reply(event.getReplyToken(), Arrays.asList(
                        new TextMessage("Chào mừng bạn đến với tính năng đăng nhập."),
                        new TextMessage("Chúng tôi cần mã nhân viên của bạn.")));
                data.setNextProcessKey(ProgressLogin.SEND_OTP);
                break;

            case SEND_OTP:
                if (event.getMessage().getText().length() == 6) {
                    lineMessengerUtils.reply(event.getReplyToken(), Arrays.asList(
                            new TextMessage("1 mã OTP đã được gửi vào ứng dụng MWG."),
                            new TextMessage("Vui lòng kiểm tra và gửi cho tôi.")));
                    data.setNextProcessKey(ProgressLogin.VERIFY_OTP);
                    break;
                }
                lineMessengerUtils.reply(event.getReplyToken(), new TextMessage("Mã nhân viên không hợp lệ xin thử lại !!!"));
                break;
                
            case VERIFY_OTP:
                if (event.getMessage().getText().equals("xxxxxx")) {
                    lineMessengerUtils.reply(event.getReplyToken(), new TextMessage("Đăng nhập thành công."));
                    LineMessengerHandler.cancelCurrentProgress(event.getSource().getUserId());
                    data.setNextProcessKey(null);
                    break;
                }
                lineMessengerUtils.reply(event.getReplyToken(), new TextMessage("Bạn đã nhập sai mã OTP vui lòng thử lại"));
                break;
        }
    }

    @Override
    public void handleStickerMessageEvent(MessageEvent<StickerMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleLocationMessageEvent(MessageEvent<LocationMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleImageMessageEvent(MessageEvent<ImageMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleAudioMessageEvent(MessageEvent<AudioMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleVideoMessageEvent(MessageEvent<VideoMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleVideoPlayCompleteEvent(VideoPlayCompleteEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleFileMessageEvent(MessageEvent<FileMessageContent> event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleUnfollowEvent(UnfollowEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleUnknownEvent(UnknownEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleFollowEvent(FollowEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleJoinEvent(JoinEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handlePostbackEvent(PostbackEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleBeaconEvent(BeaconEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleMemberJoined(MemberJoinedEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleMemberLeft(MemberLeftEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleMemberLeft(UnsendEvent event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public void handleOtherEvent(Event event, LineMessengerProgress<ProgressLogin> data) {

    }

    @Override
    public List<String> getMetaData() {
        return Arrays.asList(
                "dang nhap", "truy cap", "login", "sign in", "truy cap", "dn"
        );
    }

    @Override
    public ProgressLogin getInitState() {
        return ProgressLogin.INIT;
    }
}
