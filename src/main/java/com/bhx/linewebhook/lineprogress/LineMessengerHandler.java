package com.bhx.linewebhook.lineprogress;

import com.bhx.linewebhook.common.LineMessengerUtils;
import com.bhx.linewebhook.common.StringUtils;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerData;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerHandlerI;
import com.bhx.linewebhook.lineprogress.conifguration.LineMessengerProgress;
import com.bhx.linewebhook.lineprogress.conifguration.LineProgress;
import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@LineMessageHandler
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LineMessengerHandler {
    private static Map<String, LineMessengerData> lineBotDataByUserId = new HashMap<>();
    private static Map<String, LineMessengerHandlerI> lineMessengerHandlerByMetadata;
    private final LineMessengerUtils lineMessengerUtils;
    private final StringUtils stringUtils;

    private final ListableBeanFactory listableBeanFactory;

    @PostConstruct
    public void getHandlerMapByMetaData() {
        Map<String, Object> lineProgressComponentByName = listableBeanFactory.getBeansWithAnnotation(LineProgress.class);
        lineMessengerHandlerByMetadata = new HashMap<>();

        lineProgressComponentByName.keySet().forEach(key -> {
            LineMessengerHandlerI lineMessengerHandler = (LineMessengerHandlerI) lineProgressComponentByName.get(key);
            lineMessengerHandler.getMetaData().forEach(metaData -> {
                lineMessengerHandlerByMetadata.put(metaData.toString(), lineMessengerHandler);
            });
        });
    }

    public static void cancelCurrentProgress(String userId) {
        lineBotDataByUserId.remove(userId);
    }

    private LineMessengerData searchHandler(String messenge, int minCore) {
        if (Strings.isBlank(messenge)) return null;

        ExtractedResult searchResult = stringUtils.searchFuzzy(messenge, lineMessengerHandlerByMetadata.keySet());

        if (searchResult.getScore() < minCore) return null;

        LineMessengerHandlerI lineMessengerHandlerI = lineMessengerHandlerByMetadata.get(searchResult.getString());

        return new LineMessengerData(lineMessengerHandlerI, new LineMessengerProgress(lineMessengerHandlerI.getInitState()));
    }

    private boolean isInProcess(Event event) {
        return lineBotDataByUserId.containsKey(event.getSource().getUserId());
    }

    private LineMessengerData getHandler(Event event) {
        String userId = event.getSource().getUserId();
        if (!isInProcess(event)) return null;
        return lineBotDataByUserId.get(userId);
    }

    private boolean handleControlEvent(MessageEvent<TextMessageContent> event) {
        switch (event.getMessage().getText()) {
            case "!!":
                if (isInProcess(event)) {
                    cancelCurrentProgress(event.getSource().getUserId());
                    lineMessengerUtils.reply(event.getReplyToken(), lineMessengerUtils.getGuideLineMessage());
                }
                return true;

            case "??":
                lineMessengerUtils.reply(event.getReplyToken(), lineMessengerUtils.getGuideLineMessage());
                return true;

            default:
                return false;
        }
    }

    @EventMapping
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        log.info("event: {}", event);

        LineMessengerData lineMessengerData = null;
        String userId = event.getSource().getUserId();

        // handle control events
        if (handleControlEvent(event)) return;

        if (!isInProcess(event)) {
            lineMessengerData = this.searchHandler(event.getMessage().getText(), 90);
            if (Objects.isNull(lineMessengerData)) {
                lineMessengerUtils.reply(event.getReplyToken(), lineMessengerUtils.getGuideLineMessage());
                return;
            }
            lineBotDataByUserId.put(userId, lineMessengerData);
        }

        lineBotDataByUserId
                .get(userId)
                .getHandler()
                .handleTextMessageEvent(event, lineBotDataByUserId.get(userId).getProcess());
    }

    @EventMapping
    public void handleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleStickerMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleLocationMessageEvent(MessageEvent<LocationMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleLocationMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleImageMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleAudioMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleVideoMessageEvent(MessageEvent<VideoMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleVideoMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleVideoPlayCompleteEvent(VideoPlayCompleteEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleVideoPlayCompleteEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleFileMessageEvent(MessageEvent<FileMessageContent> event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleFileMessageEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleUnfollowEvent(UnfollowEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleUnfollowEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleUnknownEvent(UnknownEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleUnknownEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleFollowEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleJoinEvent(JoinEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleJoinEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handlePostbackEvent(PostbackEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handlePostbackEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleBeaconEvent(BeaconEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleBeaconEvent(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleMemberJoined(MemberJoinedEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleMemberJoined(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleMemberLeft(MemberLeftEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleMemberLeft(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleMemberLeft(UnsendEvent event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleMemberLeft(event, lineMessengerData.getProcess());
        }
    }

    @EventMapping
    public void handleOtherEvent(Event event) {
        log.info("event: {}", event);
        LineMessengerData lineMessengerData = getHandler(event);
        if (Objects.nonNull(lineMessengerData)) {
            lineMessengerData.getHandler().handleOtherEvent(event, lineMessengerData.getProcess());
        }
    }
}
