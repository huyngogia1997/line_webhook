package com.bhx.linewebhook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinewebhookApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinewebhookApplication.class, args);
    }
}
